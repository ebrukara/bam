﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bam.Startup))]
namespace Bam
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
